#-------------------------------------------------
#
# Project created by QtCreator 2015-05-06T17:24:41
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = Scanner
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    dbperson.cpp \
    dblabel.cpp \
    dbimage.cpp \
    dbgallerylabel.cpp \
    dbgallery.cpp \
    scanner.cpp

HEADERS += \
    dbperson.h \
    dblabel.h \
    dbimage.h \
    dbgallerylabel.h \
    dbgallery.h \
    scanner.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES +=
