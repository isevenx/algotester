#ifndef SCANNER_H
#define SCANNER_H

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfoList>
#include <QStringList>
#include <QtAlgorithms>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDateTime>
#include <QList>

#include "dbgallery.h"

using namespace std;

class Scanner
{

public:
    static Scanner& getInstance()
    {
        static Scanner instance;
        return instance;
    }
    ~Scanner();

    QString path;

    QList<DBGallery> DBGalleries;
    QList<DBGallery::GalleryInfo> galleryInfo;

    void quickScan(bool ifDebug = true);
    void deepScan(bool ifDebug = true);

    bool checkExist(QString personName, QDateTime dateTime);

private:
    Scanner();
    Scanner(Scanner const& copy);
    Scanner& operator=(Scanner const& copy);

    QSqlDatabase db;
};

#endif // SCANNER_H
