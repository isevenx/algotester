#ifndef DBPERSON_H
#define DBPERSON_H

#include <QDebug>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>
#include <QStringList>

class DBPerson
{
public:
    DBPerson();
    ~DBPerson();

    bool save();
    bool load(qint64 dbID);
    static QList<DBPerson> getList();
    static QStringList getStringList();
    inline qint64 getID(){return dbID;}

    QString name;

private:
    qint64 dbID;
};

#endif // DBPERSON_H
