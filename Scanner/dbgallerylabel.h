#ifndef DBGALLERYLABEL_H
#define DBGALLERYLABEL_H

#include <QObject>
#include <QDebug>

#include "dblabel.h"

class DBGalleryLabel
{
public:
    DBGalleryLabel();
    ~DBGalleryLabel();

    bool save();
    bool load(qint64 dbID);
    static QList<DBGalleryLabel> getList(qint64 galleryID);
    inline qint64 getID(){return dbID;}

    DBLabel label;
    int gallery_id;
    bool ifDelete;

private:    
    qint64 dbID;

};

#endif // DBGALLERYLABEL_H
