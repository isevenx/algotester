#ifndef DBLABEL_H
#define DBLABEL_H

#include <QDebug>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>
#include <QStringList>

class DBLabel
{
public:
    DBLabel();
    ~DBLabel();

    bool save();
    bool load(qint64 dbID);
    static QList<DBLabel> getList();
    static QStringList getStringList();
    inline qint64 getID(){return dbID;}

    QString name;

private:
    qint64 dbID;
};

#endif // DBLABEL_H
