#include "dbgallerylabel.h"

DBGalleryLabel::DBGalleryLabel()
{
    this->dbID = -1;
    this->ifDelete = false;
}

DBGalleryLabel::~DBGalleryLabel()
{

}

bool DBGalleryLabel::save()
{
    QSqlQuery query;
    if(ifDelete && dbID != -1){
        query.prepare("DELETE FROM GalleryLabels WHERE id = :id");
        query.bindValue(":id",dbID);
        if(query.exec()) return true;
        else return false;
    }

    if(!label.save())
        return false;

    if(dbID == -1){
        query.prepare("INSERT INTO GalleryLabels(gallery_id, label_id) VALUES(:gallery_id, :label_id)");
        query.bindValue(":gallery_id", gallery_id);
        query.bindValue(":label_id", label.getID());
        if(!query.exec()) return false;
    }

    if(dbID == -1) dbID = query.lastInsertId().toInt();
    return true;
}

bool DBGalleryLabel ::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM GalleryLabels WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->gallery_id = query.value("gallery_id").toInt();
        this->label.load(query.value("label_id").toInt());
        this->dbID = dbID;
        return true;
    }
    return false;
}

QList<DBGalleryLabel> DBGalleryLabel::getList(qint64 galleryID)
{
    QList<DBGalleryLabel> result;

    QSqlQuery query;
    query.prepare("SELECT * FROM GalleryLabels WHERE gallery_id = :gallery_id");
    query.bindValue(":gallery_id", galleryID);
    if(!query.exec()) return result;

    while(query.next()){
        DBGalleryLabel tGLabel;
        tGLabel.load(query.value("id").toInt());
        result.append(tGLabel);
    }
    return result;
}
