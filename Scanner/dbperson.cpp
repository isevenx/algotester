#include "dbperson.h"

DBPerson::DBPerson()
{
    this->dbID = -1;
}

DBPerson::~DBPerson()
{

}

bool DBPerson::save()
{
    QSqlQuery query;
    if(dbID == -1){
        QSqlQuery qry;
        qry.prepare("SELECT * FROM Persons WHERE name = :name");
        qry.bindValue(":name", name);
        if(qry.exec() && qry.first()){
            dbID = qry.value("id").toInt();
            return true;
        }
        else{
            query.prepare("INSERT INTO Persons(name) VALUES(:name)");
        }
    }
    else{
        query.prepare("UPDATE Persons SET "
                      "name = :name "
                      "WHERE id = :id");
        query.bindValue(":id", dbID);
    }
    query.bindValue(":name", name);
    if(!query.exec()) return false;


    if(dbID == -1) dbID = query.lastInsertId().toInt();
    return true;
}

bool DBPerson::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Persons WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->name = query.value("name").toString();
        this->dbID = dbID;
        return true;
    }
    return false;
}

QList<DBPerson> DBPerson::getList()
{
    QList<DBPerson> result;

    QSqlQuery query;
    query.prepare("SELECT * FROM Persons");
    if(!query.exec()) return result;

    while(query.next()){
        DBPerson tPerson;
        tPerson.load(query.value("id").toInt());
        result.append(tPerson);
    }
    return result;
}

QStringList DBPerson::getStringList()
{
    QStringList result;

    QSqlQuery query;
    query.prepare("SELECT * FROM Persons");
    if(!query.exec()) return result;

    while(query.next()){
        DBPerson tPerson;
        tPerson.load(query.value("id").toInt());
        result.append(tPerson.name);
    }
    return result;

}
