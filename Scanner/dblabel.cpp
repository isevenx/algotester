#include "dblabel.h"

DBLabel::DBLabel()
{
    this->dbID = -1;

}

DBLabel::~DBLabel()
{

}

bool DBLabel::save()
{
    QSqlQuery query;
    if(dbID == -1){
        QSqlQuery qry;
        qry.prepare("SELECT * FROM Labels WHERE name = :name");
        qry.bindValue(":name", name);
        if(qry.exec() && qry.first()){
            dbID = qry.value("id").toInt();
            return true;
        }
        query.prepare("INSERT INTO Labels(name) VALUES(:name)");
    }
    else{
        query.prepare("UPDATE Labels SET "
                      "name = :name "
                      "WHERE id = :id");
        query.bindValue(":id", dbID);
    }
    query.bindValue(":name", name);
    if(!query.exec()) return false;

    if(dbID == -1) dbID = query.lastInsertId().toInt();
    return true;
}

bool DBLabel::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Labels WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->name = query.value("name").toString();
        this->dbID = dbID;
        return true;
    }
    return false;
}

QList<DBLabel> DBLabel::getList()
{
    QList<DBLabel> result;

    QSqlQuery query;
    query.prepare("SELECT * FROM Labels");
    if(!query.exec()) return result;

    while(query.next()){
        DBLabel tLabel;
        tLabel.load(query.value("id").toInt());
        result.append(tLabel);
    }
    return result;
}

QStringList DBLabel::getStringList()
{
    QStringList result;

    QSqlQuery query;
    query.prepare("SELECT * FROM Labels");
    if(!query.exec()) return result;

    while(query.next()){
        DBLabel tLabel;
        tLabel.load(query.value("id").toInt());
        result.append(tLabel.name);
    }
    return result;
}
