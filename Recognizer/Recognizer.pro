#-------------------------------------------------
#
# Project created by QtCreator 2015-04-21T10:52:04
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = Recognizer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

SOURCES += main.cpp

HEADERS +=

include(Algoritms/Algoritms.pri)
include(Proc/Proc.pri)
#include(Struct/Struct.pri)

unix:!macx: LIBS += -L$$OUT_PWD/../Scanner/ -lScanner

INCLUDEPATH += $$PWD/../Scanner
DEPENDPATH += $$PWD/../Scanner

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Scanner/libScanner.a
