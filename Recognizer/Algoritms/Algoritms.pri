INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/ialgo.h \
    $$PWD/eigenalg.h \
    $$PWD/fisheralg.h \
    $$PWD/lbphalg.h

SOURCES += \
    $$PWD/ialgo.cpp \
    $$PWD/eigenalg.cpp \
    $$PWD/fisheralg.cpp \
    $$PWD/lbphalg.cpp
