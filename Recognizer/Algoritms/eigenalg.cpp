#include "eigenalg.h"

EigenAlg::EigenAlg()
{
    model = createEigenFaceRecognizer(0, 3200);
}

EigenAlg::~EigenAlg()
{

}

void EigenAlg::setPersons(int count)
{
    personCount = count;
}

bool EigenAlg::addFace(Mat face, int label)
{
    if(face.empty() || label < 0 || label > personCount-1)
        return false;

//    Mat temp;
//    resize(face, temp, Size(100, 100));
//    cvtColor(temp, face, CV_BGR2GRAY);
    face = equalize(face);
    faces.push_back(face);
    labels.push_back(label);
    return true;
}

bool EigenAlg::learn()
{
    if(faces.empty() || faces.size() != labels.size())
        return false;

    model->train(faces, labels);
    return true;
}

int EigenAlg::testFace(Mat testFace)
{
    testFace = equalize(testFace);
    return model->predict(testFace);
}

Mat EigenAlg::equalize(Mat inMat)
{
    Mat dst, src, temp;

    resize(inMat, temp, Size(100, 100));
    if(temp.type() != CV_8UC1)
        cvtColor(temp, src, CV_BGR2GRAY);
    else
        src = temp;
    dst = src.clone();

    uchar * dst_p, * dst_end;
    long histogram[256] = {0};
    for( dst_p = dst.data,
         dst_end = dst.data + dst.rows * dst.cols;
         dst_p != dst_end; ++dst_p)
    {
        histogram[*dst_p]++;
    }

    long darkPix = 0, lightPix = 0;
    for(int i = 0; i < 256; i++){
        if(i < 31)
            darkPix += histogram[i];
        else
            lightPix += histogram[i];
    }
    if(double(darkPix / lightPix) > 1.5){
        Ptr<CLAHE> clahe = createCLAHE(10.0,Size(9,9));
        clahe->apply(src,dst);

        for(int i=0;i<dst.cols;i++){
            for(int j=0;j<dst.rows;j++){
                double num  = (1.0+((dst.at<uchar>(Point(i,j))-128.0)/256.0))*dst.at<uchar>(Point(i,j));
                num = dst.at<uchar>(Point(i,j)) - num;
                int hope = (int)(num*DELTA_PRIMARY);
                dst.at<uchar>(Point(i,j)) += hope;
            }
        }
    }

    return dst;

//    Mat white(dst.rows, dst.cols, CV_8UC1, Scalar(255));
//    cv::ellipse(white, Point(dst.cols/2, dst.rows/2), Size(dst.rows/2.3, dst.cols/2.3), 0, 0, 360, Scalar(0), -1, 8 );
//    Mat result;
//    bitwise_or(dst, white, result);
//    return result;
}

