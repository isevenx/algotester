#ifndef IALGO_H
#define IALGO_H

#include <QObject>
#include <QDebug>
#include <QTime>
#include <QStringList>

#include <opencv2/opencv.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

using namespace cv;
using namespace std;

class iAlgo : public QObject
{
    Q_OBJECT
public:
    explicit iAlgo(QObject *parent = 0);
    ~iAlgo();

    ///
    /// \brief setPersons sets total number of persons in model
    /// \param count
    ///
    virtual void setPersons(int count);
    ///
    /// \brief addFace adds face to model
    /// \param face Image of face
    /// \param label person nr (0 .. setPersons(count)-1 )
    /// \return
    ///
    virtual bool addFace(Mat face, int label);
    ///
    /// \brief addNegFace adds persons face or object that is negative sample
    /// \param negFace
    ///
    virtual void addNegFace(Mat negFace);
    ///
    /// \brief learn combines all faces added by addFace and addNegFace to train model
    /// \return
    ///
    virtual bool learn();
    ///
    /// \brief testFace predicts person by face image on already trained model
    /// \param testFace
    /// \return label used in addFace function if algorithm has corectly prediced face, -1 if unpredicted
    ///
    virtual int testFace(Mat testFace);
    ///
    /// \brief clear clears model and all faces
    ///
    virtual void clear();

};

#endif // IALGO_H
