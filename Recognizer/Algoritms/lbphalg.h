#ifndef LBPHALG_H
#define LBPHALG_H

#include <QObject>
#include "ialgo.h"

class LBPHAlg : public iAlgo
{
public:
    LBPHAlg();
    ~LBPHAlg();

    virtual void setPersons(int count);
    virtual bool addFace(Mat face, int label);
    virtual bool learn();
    virtual int testFace(Mat testFace);

private:
    Mat equalize(Mat inMat);

    int personCount;
    vector<Mat> faces;
    vector<int> labels;
    Ptr<FaceRecognizer> model;
    static const double DELTA_PRIMARY = 0.7;
};

#endif // LBPHALG_H
