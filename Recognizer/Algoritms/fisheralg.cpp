#include "fisheralg.h"

FisherAlg::FisherAlg()
{
    model = createFisherFaceRecognizer(0, 460);
}

FisherAlg::~FisherAlg()
{

}

void FisherAlg::setPersons(int count)
{
    personCount = count;
}

bool FisherAlg::addFace(Mat face, int label)
{
    if(face.empty() || label < 0 || label > personCount-1)
        return false;

//    Mat temp;
//    resize(face, temp, Size(100, 100));
//    cvtColor(temp, face, CV_BGR2GRAY);
    face = equalize(face);
    faces.push_back(face);
    labels.push_back(label);
    return true;
}

bool FisherAlg::learn()
{
    if(faces.empty() || faces.size() != labels.size())
        return false;

    model->train(faces, labels);
    return true;
}

int FisherAlg::testFace(Mat testFace)
{
    testFace = equalize(testFace);
    return model->predict(testFace);
}

Mat FisherAlg::equalize(Mat inMat)
{
    Mat dst, src, temp;

    resize(inMat, temp, Size(100, 100));
    if(temp.type() != CV_8UC1)
        cvtColor(temp, src, CV_BGR2GRAY);
    else
        src = temp;

    dst = src.clone();

//    Ptr<CLAHE> clahe = createCLAHE(10.0,Size(10,10));
//    clahe->apply(src,dst);

//    for(int i=0;i<dst.cols;i++){
//        for(int j=0;j<dst.rows;j++){
//            double num  = (1.0+((dst.at<uchar>(Point(i,j))-128.0)/256.0))*dst.at<uchar>(Point(i,j));
//            num = dst.at<uchar>(Point(i,j)) - num;
//            int hope = (int)(num*DELTA_PRIMARY);
//            dst.at<uchar>(Point(i,j)) += hope;
//        }
//    }
//    return dst;

    Mat white(dst.rows, dst.cols, CV_8UC1, Scalar(255));
    cv::ellipse(white, Point(dst.cols/2, dst.rows/2), Size(dst.rows/2.3, dst.cols/2.3), 0, 0, 360, Scalar(0), -1, 8 );
    Mat result;
    bitwise_or(dst, white, result);
    return result;
}
