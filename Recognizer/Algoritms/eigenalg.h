#ifndef EIGENALG_H
#define EIGENALG_H

#include <QObject>
#include "ialgo.h"

class EigenAlg : public iAlgo
{
public:
    EigenAlg();
    ~EigenAlg();

    virtual void setPersons(int count);
    virtual bool addFace(Mat face, int label);
    virtual bool learn();
    virtual int testFace(Mat testFace);

private:
    Mat equalize(Mat inMat);

    int personCount;
    vector<Mat> faces;
    vector<int> labels;
    Ptr<FaceRecognizer> model;
    static const double DELTA_PRIMARY = 0.7;
};

#endif // EIGENALG_H
