#ifndef FISHERALG_H
#define FISHERALG_H

#include <ialgo.h>
#include <QObject>

class FisherAlg : public iAlgo
{
public:
    FisherAlg();
    ~FisherAlg();

    virtual void setPersons(int count);
    virtual bool addFace(Mat face, int label);
    virtual bool learn();
    virtual int testFace(Mat testFace);

private:
    Mat equalize(Mat inMat);

    int personCount;
    vector<Mat> faces;
    vector<int> labels;
    Ptr<FaceRecognizer> model;
    static const double DELTA_PRIMARY = 0.7;
};

#endif // FISHERALG_H
