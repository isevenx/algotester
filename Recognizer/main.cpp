#include <QCoreApplication>
#include <QTime>

#include "scanner.h"
#include "queryreader.h"
#include "fisheralg.h"
#include "eigenalg.h"
#include "lbphalg.h"

enum algTypes{
    FISHER=0,
    EIGEN=1,
    LBPH=2
};

int main(int argc, char *argv[])
{

    // ========== Arguments ==================
    QString stdPath = QDir::homePath() + "/Pictures/Person_Images";
    bool ifDebug = false;
    bool ifDeep = false;
    int algnr = 0;

    for(int i = 0; i < argc; i++){
        if(strcmp( argv[i], "-stdPath") == 0)
            stdPath = argv[++i];
        else if(strcmp( argv[i], "--debug") == 0)
            ifDebug = true;
        else if(strcmp( argv[i], "--deep") == 0)
            ifDeep = true;
        else if(strcmp( argv[i], "-alg") == 0)
            algnr = atoi(argv[++i]);
    }    
    QString queryFile = stdPath+"/query.json";

    // ========== Initialize =================
    Scanner *scanner = &Scanner::getInstance();
    QueryReader qReader(queryFile);
    iAlgo *ialgo = 0;

    switch(algnr){
        case FISHER: ialgo = new FisherAlg(); break;
        case EIGEN: ialgo = new EigenAlg(); break;
        case LBPH: ialgo = new LBPHAlg(); break;
    }
    if(ialgo == 0) return -1;

    QTime bench;

    // ========== Scan file system ===========
    if(ifDeep) scanner->deepScan(ifDebug);
    else scanner->quickScan(ifDebug);


    // ========== Set person count ===========
    QStringList personList = qReader.getPersons();
    ialgo->setPersons(personList.size());
    if(ifDebug) qDebug() << "===== LEARN SET: " << personList << "\n";


    // ========== Train algorithm ============
    int learntime;
    bench.start();
    QList< QPair<QString, int> > learnSet = qReader.getLearnSet();
    for(QList< QPair<QString, int> >::iterator it = learnSet.begin(); it < learnSet.end(); ++it){
        Mat face = imread(it->first.toStdString());
        ialgo->addFace(face, it->second);
    }
    ialgo->learn();
    learntime = bench.elapsed();


    // ========== Test algorithm =============

    QJsonObject mainObject, timings;
    QJsonArray results;

    int testtime, correctCount = 0;

    bench.start();
    QList< QPair<QString, QString> > testSet = qReader.getTestSet();
    for(QList< QPair<QString, QString> >::iterator it = testSet.begin(); it < testSet.end(); ++it){

        // ===== Make predictions
        Mat face = imread(it->first.toStdString());
        int prediction = ialgo->testFace(face);

        // ===== Make results
        {
            // ===== Find object
            QJsonObject result;
            foreach(const QJsonValue &value, results){
                QJsonObject obj = value.toObject();
                QString name = obj["Name"].toString();
                if(name == it->second){
                    result = obj;
                    break;
                }
            }

            // ===== Create if not found
            if(result["Name"].toString() != it->second){
                result["Name"] = it->second;
                result["Correct"] = 0;
                result["Invalid"] = 0;
                result["Unrec_!Trained"] = 0;
                result["Unrec_Trained"] = 0;
                results.append(result);
            }

            // ===== Change results
            if(prediction == -1){
                if(personList.indexOf(it->second) == -1){
                    result["Unrec_!Trained"] = result["Unrec_!Trained"].toDouble() + 1;
                    correctCount++;
                }
                else
                    result["Unrec_Trained"] = result["Unrec_Trained"].toDouble() + 1;
            }
            else{
                if(personList[prediction] == it->second){
                    result["Correct"] = result["Correct"].toDouble() + 1;
                    correctCount++;
                }
                else
                    result["Invalid"] = result["Invalid"].toDouble() + 1;
            }

            int resultID = 0;
            foreach(const QJsonValue &value, results){
                QJsonObject obj = value.toObject();
                QString name = obj["Name"].toString();
                if(name == it->second){
                    results.replace(resultID, result);
                    break;
                }
                resultID++;
            }
        }
//        qDebug() << it->second << prediction << it->first.right(it->first.length() - it->first.lastIndexOf("/") - 1);
    }
    testtime = bench.elapsed();


    // ========== Save results ===============
    mainObject["Results"] = results;
    mainObject["Correct%"] = correctCount / (double)testSet.size()*100;

    timings["AVG LEARN(ms)"] = (double)learntime / learnSet.size();
    timings["AVG TEST(ms)"] = (double)testtime / testSet.size();
    timings["OVERALL LEARN(ms)"] = learntime;
    timings["OVERALL TEST(ms)"] = testtime;

    mainObject["Timings"] = timings;

    QFile resFile(stdPath+"/results_"+QString::number(algnr)+".json");
    resFile.open(QIODevice::WriteOnly);
    resFile.write(QJsonDocument(mainObject).toJson());
    resFile.close();

    if(ifDebug){
        qDebug() << "===== ALGORITHM" << algnr << "TEST RESULTS: \n";
        foreach(const QJsonValue &value, results){
            QJsonObject obj = value.toObject();
            qDebug() << "Name: " << obj["Name"].toString() << "\n      |                  Correct: " << obj["Correct"].toDouble()\
                     << "\n      |                  Invalid: " << obj["Invalid"].toDouble()\
                     << "\n      | Unrecognized not trained: " << obj["Unrec_!Trained"].toDouble()\
                     << "\n      | Unrecognized     trained: " << obj["Unrec_Trained"].toDouble()\
                     << "\n";
        }

        qDebug() << "Correct results %  : " << mainObject["Correct%"].toDouble() << "%";
        qDebug() << "";
        qDebug() << "Average learn time : " << timings["AVG LEARN(ms)"].toDouble() << "ms";
        qDebug() << "Learn overall time : " << timings["OVERALL LEARN(ms)"].toDouble() << " ms";
        qDebug() << "Average test time  : " << timings["AVG TEST(ms)"].toDouble() << "ms";
        qDebug() << "Test overall time  : " << timings["OVERALL TEST(ms)"].toDouble() << " ms";
        qDebug() << "";
    }

    return 0;
}
