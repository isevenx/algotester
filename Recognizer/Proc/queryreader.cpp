#include "queryreader.h"

QueryReader::QueryReader(QString filePath)
{
    path = QDir::homePath()+"/Pictures/Person_Images";
    setFilePath(filePath);
}

QueryReader::~QueryReader()
{

}

void QueryReader::setFilePath(QString filePath)
{
    this->filePath = filePath;
}

QStringList QueryReader::getPersons()
{
    QStringList result;

    QFile jFile(filePath);
    if(jFile.open(QIODevice::ReadOnly)){

        QString jsonFile = jFile.readAll();
        QJsonDocument mainDoc = QJsonDocument::fromJson(jsonFile.toUtf8());
        QJsonObject mainObject;
        mainObject = mainDoc.object();

        QJsonArray persons = mainObject["Persons"].toArray();
        foreach(const QJsonValue &value, persons){
            QJsonObject obj = value.toObject();
            QString person = obj["Name"].toString();
            result.append(person);
        }
    }

    return result;
}

QList< QPair<QString, int> > QueryReader::getLearnSet()
{
    QList< QPair<QString, int> > result;

    QFile jFile(filePath);
    if(jFile.open(QIODevice::ReadOnly)){

        QString jsonFile = jFile.readAll();
        QJsonDocument mainDoc = QJsonDocument::fromJson(jsonFile.toUtf8());
        QJsonObject mainObject;
        mainObject = mainDoc.object();

        int personID = 0;
        QJsonArray persons = mainObject["Persons"].toArray();
        foreach(const QJsonValue &value, persons){
            QJsonObject obj = value.toObject();
            QString person = obj["Name"].toString();
            QString qry = obj["Query"].toString();

            QSqlQuery query;
            query.prepare(qry);

            if(!query.exec())
                return result;

            while(query.next()){
                QString pname = query.value("pname").toString();
                if(pname != person)
                    continue;
                QDateTime datetime = query.value("date").toDateTime();
                QString filename = query.value("filename").toString();
                QString tString = path + "/" + pname + "/" + datetime.toString("yyyy.MM.dd_hh.mm") + "/" + filename;

                QPair<QString, int> faceInfo;
                faceInfo.first = tString;
                faceInfo.second = personID;
                result.append(faceInfo);
            }
            personID++;
        }
    }

    return result;
}

QStringList QueryReader::getNegativeSet()
{
    QStringList result;

    QFile jFile(filePath);
    if(jFile.open(QIODevice::ReadOnly)){

        QString jsonFile = jFile.readAll();
        QJsonDocument mainDoc = QJsonDocument::fromJson(jsonFile.toUtf8());
        QJsonObject mainObject;
        mainObject = mainDoc.object();

        QString qry = mainObject["Negative Set"].toString();

        QSqlQuery query;
        query.prepare(qry);

        if(!query.exec())
            return result;

        while(query.next()){
            QString pname = query.value("pname").toString();
            QDateTime datetime = query.value("date").toDateTime();
            QString filename = query.value("filename").toString();

            QString tString = path + "/" + pname + "/" + datetime.toString("yyyy.MM.dd_hh.mm") + "/" + filename;
            result.append(tString);
        }
    }

    return result;
}

QList< QPair<QString, QString> > QueryReader::getTestSet()
{
    QList< QPair<QString, QString> > result;

    QFile jFile(filePath);
    if(jFile.open(QIODevice::ReadOnly)){

        QString jsonFile = jFile.readAll();
        QJsonDocument mainDoc = QJsonDocument::fromJson(jsonFile.toUtf8());
        QJsonObject mainObject;
        mainObject = mainDoc.object();

        QString qry = mainObject["Test Set"].toString();

        QSqlQuery query;
        query.prepare(qry);

        if(!query.exec())
            return result;

        while(query.next()){
            QString pname = query.value("pname").toString();
            QDateTime datetime = query.value("date").toDateTime();
            QString filename = query.value("filename").toString();

            QPair<QString, QString> testInfo;
            testInfo.first = path + "/" + pname + "/" + datetime.toString("yyyy.MM.dd_hh.mm") + "/" + filename;
            testInfo.second = pname;
            result.append(testInfo);
        }
    }

    return result;
}

