#ifndef QUERYREADER_H
#define QUERYREADER_H

#include <QObject>
#include <QDir>
#include <QDebug>
#include <QDateTime>

#include <QJsonValue>
#include <QJsonArray>
#include <QJsonValueRef>
#include <QJsonObject>
#include <QJsonDocument>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

class QueryReader : public QObject
{
    Q_OBJECT
public:
    explicit QueryReader(QString filePath = "");
    ~QueryReader();
    void setFilePath(QString filePath);

    QStringList getPersons();
    QList< QPair<QString, int> > getLearnSet();
    QStringList getNegativeSet();
    QList< QPair<QString, QString> > getTestSet();

signals:

public slots:

private:
    QString path, filePath;
};

#endif // QUERYREADER_H
