#-------------------------------------------------
#
# Project created by QtCreator 2015-04-21T10:24:47
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GalleryViewer
TEMPLATE = app

LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

SOURCES += main.cpp\
        gallerywindow.cpp\
    personwindow.cpp

HEADERS  += gallerywindow.h\
    personwindow.h

FORMS    += gallerywindow.ui\
    personwindow.ui

include(Proc/Proc.pri)
#include(Hardware/Hardware.pri)
#include(Struct/Struct.pri)
include(Widgets/Widgets.pri)
#include(Struct2/Struct2.pri)

unix:!macx: LIBS += -L$$OUT_PWD/../Scanner/ -lScanner

INCLUDEPATH += $$PWD/../Scanner
DEPENDPATH += $$PWD/../Scanner

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../Scanner/libScanner.a

RESOURCES += \
    src.qrc
