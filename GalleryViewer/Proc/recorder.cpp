#include "recorder.h"

Recorder::Recorder(QObject *parent) : QObject(parent)
{

}

Recorder::~Recorder()
{

}

void Recorder::addImage(Mat img)
{
    DBImage tImage;
    tImage.setMat(img);
    gallery.addImage(tImage);
}

void Recorder::newRecord()
{
    gallery = DBGallery();
    gallery.person.name = "Unknown";
    gallery.dateTime = QDateTime(QDateTime::currentDateTime().date(),
                                 QTime(QDateTime::currentDateTime().time().hour(), QDateTime::currentDateTime().time().minute(),0,0));
    gallery.name = "Unknown / " + gallery.dateTime.toString("dd MMM hh") + ". stunda";
    gallery.save();

    DBGalleryLabel tGLabel;
    tGLabel.label.name = "None";
    tGLabel.gallery_id = gallery.getID();
    gallery.DBLabels.append(tGLabel);
}

