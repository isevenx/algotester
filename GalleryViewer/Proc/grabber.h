#ifndef GRABBER_H
#define GRABBER_H

#include <QObject>
#include <QDebug>
#include <QTime>
#include <QTimer>


#include <opencv2/opencv.hpp>

using namespace cv;

class Grabber : public QObject
{
    Q_OBJECT
public:
    explicit Grabber(QObject *parent = 0);
    ~Grabber();

signals:
    void sendFrame(Mat frame, int frameNr);

public slots:
    void startGrab();
    void stopGrab();
    void grab();

private:
    VideoCapture *cap;

    bool ifStop;
    int FPS, currentFrame;
};

#endif // GRABBER_H
