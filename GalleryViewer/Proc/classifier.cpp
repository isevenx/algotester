#include "classifier.h"

Classifier::Classifier(QObject *parent) : QObject(parent)
{
    cascade = new CascadeClassifier();
    QFile file(":/cascade/haarcascade_frontalface_alt.xml");
    if(!QFileInfo(QDir::current().absoluteFilePath("cascade.xml")).exists())
        file.copy(QDir::current().absoluteFilePath("cascade.xml"));
    cascade->load("cascade.xml");
}

Classifier::~Classifier()
{

}

void Classifier::detectFaces(Mat frame)
{
    vector<Rect> faceVec;
    cascade->detectMultiScale(frame,faceVec,1.3,3,3|CV_HAAR_SCALE_IMAGE,Size(60,60));
    emit sendFaces(frame, faceVec);
}
