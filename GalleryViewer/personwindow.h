#ifndef PERSONWINDOW_H
#define PERSONWINDOW_H

#include <QSettings>
#include <QMainWindow>
#include <QWidget>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QDialogButtonBox>

#include "personcapture.h"
#include "personedit.h"
#include "scanner.h"
#include "imagesettings.h"

namespace Ui {
class PersonWindow;
}

class PersonWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PersonWindow(QWidget *parent = 0);
    ~PersonWindow();

signals:
    void saveOne(qint64 curGallery);
    void updateInfo();

public slots:
    void showCapture();
    void showEdit(qint64 dbID);

private:
    Ui::PersonWindow *ui;

    ImageSettings *imageSettings;
    PersonCapture *pCapture;
    PersonEdit *pEdit;
    Scanner *scanner;

    void closeEvent(QCloseEvent*);

private slots:
    void onWidgetChange(int ID);
};

#endif // PERSONWINDOW_H
