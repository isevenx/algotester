#include "clicklabel.h"

ClickLabel::ClickLabel(QWidget *parent, const int &ID) : QLabel(parent)
{
    this->ID = ID;
}

ClickLabel::~ClickLabel()
{

}

void ClickLabel::mousePressEvent(QMouseEvent *)
{
    emit clicked(ID);
}

void ClickLabel::click()
{
    emit clicked(ID);
}

void ClickLabel::setTextPixmap(QString text)
{
    QFont font("Times", 15);
    QFontMetrics fm(font);
    int pixelsWide = fm.width(text)+30;
    int pixelsHigh = fm.height()+5;

    QPixmap pixmap(pixelsWide,pixelsHigh);
    pixmap.fill(Qt::transparent);
    QPen pen(Qt::darkGray);
    pen.setWidth(2);

    QPainter painter(&pixmap);
    painter.setPen(pen);
    painter.setFont(font);
    painter.drawRoundedRect(2,2,pixelsWide-3, pixelsHigh-3,5,5);
    painter.drawText(QPoint(7, fm.height()-4), text);
    painter.setFont(QFont("Times", 12));
    painter.drawText(QPoint(pixelsWide-15, fm.height()-5), "x");
    this->setPixmap(pixmap);
}

