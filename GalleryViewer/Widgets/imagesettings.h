#ifndef IMAGESETTINGS_H
#define IMAGESETTINGS_H

#include <QWidget>
#include <QSettings>

namespace Ui {
class ImageSettings;
}

class ImageSettings : public QWidget
{
    Q_OBJECT

public:
    explicit ImageSettings(QWidget *parent = 0);
    ~ImageSettings();
signals:
    void applySettings();

public slots:
    void takeSettings();

private:
    Ui::ImageSettings *ui;
    QSettings settings;

    void closeEvent(QCloseEvent *);

};

#endif // IMAGESETTINGS_H
