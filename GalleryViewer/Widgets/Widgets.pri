INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

SOURCES += $$PWD/personcapture.cpp\
        $$PWD/personedit.cpp\
    $$PWD/flowlayout.cpp \
    $$PWD/imagesettings.cpp \
    $$PWD/clicklabel.cpp
    
HEADERS  += $$PWD/personcapture.h\
         $$PWD/personedit.h\
    $$PWD/flowlayout.h \
    $$PWD/imagesettings.h \
    $$PWD/clicklabel.h
   
FORMS    += $$PWD/personcapture.ui\
         $$PWD/personedit.ui \
    $$PWD/imagesettings.ui
    
